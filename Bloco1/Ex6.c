/*
Ex 6: Ler nome, sexo e idade. Se sexo for feminino e idade menor que 25, imprime o nome
da pessoa e a palavra "ACEITA." caso contrário imprimir "NÃO ACEITA."
*/

#include <stdio.h>

char nome [20], sexo;
int idade, i;

int main () {

printf ("Digite nome: ");
fgets (nome, sizeof(nome), stdin);

i = 0;
while (nome [i] != '\0') {
    if (nome [i] == '\n') {
    nome[i] = '\0';
    break;
    }
i++;
}

printf ("Digite sexo (F/f para feminino e M/m para masculino): ");
scanf ("%c", &sexo);


printf ("Digite idade: ");
scanf ("%d", &idade);

if ((sexo == 'f' || sexo == 'F') && idade <25 ) {
    printf ("%s, ", nome);
    printf ("ACEITA.");
} else {
    printf ("NÃO ACEITA.");
}
return 0;
}