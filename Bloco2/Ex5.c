/*
Ex 5. Leia um vetor de 10 posições. Contar e escrever quantos valores pares ele possui.
*/
#include <stdio.h>

int vetor [10], i = 0, pares;

int main () {
    for (i=1; i<=10; i++) {
    printf ("Digite o %dº número: ", i);
    scanf ("%d", &vetor [i-1]);
    }

    for (i=0; i<10; i++) {
        if (vetor [i] % 2 == 0) {
            pares++;
        }
    }

printf ("Numeros pares encontrados: %d", pares);
}