/*
Ex 8. Crie um programa que lê 6 valores inteiros e, em seguida, mostre na tela os valores lidos na
ordem inversa.
*/

#include <stdio.h>

int i, vetor [6];

int main () {

    for (i=1; i<=6; i++) {
    printf ("Digite o %dº número: ", i);
    scanf ("%d", &vetor [i-1]);
    }

printf ("Ordem inversa:\n");

/*
printf ("%d\n", vetor [0]);
printf ("%d\n", vetor [1]);
printf ("%d\n", vetor [2]);
printf ("%d\n", vetor [3]);
printf ("%d\n", vetor [4]);
printf ("%d\n", vetor [5]);
*/

    for (i = 5; i >= 0; --i) {
        printf ("%d\n", vetor [i]);
    }
return 0;
}