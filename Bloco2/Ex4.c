/*
Faça um programa que leia um vetor de 8 posições, e em seguida, leia também dois valores X e Y quaisquer correspondentes
a duas posições no vetor. Ao final seu programa deverá escrever a soma dos valores encontrados nas respectivas posições X e Y
*/
#include <stdio.h>

int i, vetor[8], valX, valY;

int main () {

for (i=0; i<8; i++) {
    printf ("Digite um numero: ");
    scanf ("%d", &vetor[i]);
}

printf ("Digite X: \n"); 
    scanf ("%d", &valX);


printf ("Digite Y: \n");
    scanf ("%d", &valY);


int soma = 0;

    if (valX <= valY) {
        while (valX <= valY) { 
        soma = soma + vetor [valX];
        valX++;
        }
    } else {
        while (valY <= valX) { 
        soma = soma + vetor [valY];
        valY++;
        }
    }
    
printf ("Soma: %d", soma);
return 0;
}
