/*
Ex 6. Faça um programa que receba do usuário um vetor com 10 posições. Em seguida deverá
ser impresso o maior e o menor elemento do vetor.
*/
#include <stdio.h>

int vetor [10], i = 0, maior, menor;

int main () {

    printf ("Digite o 1º número: ");
    scanf ("%d", &vetor [i]);

    maior = vetor [i];
    menor = vetor [i];

    for (i=2; i<=10; i++) {
    printf ("Digite o %dº número: ", i);
    scanf ("%d", &vetor [i-1]);
    }

    for (i=0; i<10; i++) {
        if (vetor [i] > maior) {
            maior = vetor [i];
        }

        if (vetor [i] < menor) {
            menor = vetor [i];
        }
    }

printf ("Maior: %d\n", maior);
printf ("Menor: %d", menor);
return 0;
}