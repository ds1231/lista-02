/*
Crie um programa que lê 6 valores inteiros, e em seguida, mostre na tela os valores lidos.
*/

#include <stdio.h>

int num, i, vetor [6];

int main () {

for (i=1; i<=6; i++) {
    printf ("Digite o %dº numero: ", i);
    scanf ("%d", &num);
    vetor [i-1] = num;
}

for (i=0; i<6; i++) {
    printf ("%d\n", vetor [i]);
}

return 0;
}