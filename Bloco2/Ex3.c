/*
Ex 3. Ler um conjunto de numeros reais, armazenando-o em vetor e calcular
o quadrado das componentes deste vetor, armazenando o resultado em outro vetor.
Os conjuntos têm 10 elementos cada.
Imprimir todos os conjuntos.
*/

#include <stdio.h>

int i;
float vetor1 [10], vetor2[10];

int main () {

for (i=0; i<10; i++) {
    printf ("Digite o numero float: ");
    scanf ("%f", &vetor1 [i]);
}

for (i=0; i<10; i++) {
    vetor2[i] = vetor1[i] * vetor1 [i];
}

printf ("Vetor 1:\n");
for (i=0; i<10; i++){

printf ("%f\n", vetor1[i]);
}

printf ("Vetor 2:\n");
for (i=0; i<10; i++) {

printf ("%f\n", vetor2[i]);
}

return 0;
}